<?php

namespace Modules\YindulaCore\Utilities\MediaLibrary;


use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator as BasePathGenerator;

/**
 * Class CustomPathGenerator
 */
class CustomPathGenerator implements BasePathGenerator
{

    protected function getMediaId($media)
    {
        return md5($media->id . config('app.key'));
    }

    /**
     *
     * Description for function
     *
     * @param  type  $media Description
     *
     * @return string
     *
     */
    protected function collectionName($media, $path): string
    {
        return str_replace('{PARENT_DIR}', $media->collection_name, $path);
    }

    /**
     * getPath
     *
     * @param  Media $media
     * @return string
     */
    public function getPath(Media $media): string
    {
        $path = '{PARENT_DIR}' . DIRECTORY_SEPARATOR . $this->getMediaId($media) . DIRECTORY_SEPARATOR;

        return $this->collectionName($media, $path);
    }

    /**
     * @param  Media  $media
     * @return string
     */
    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media) . 'thumbnails/';
    }

    /**
     * getPathForResponsiveImages
     *
     * @param  Media $media
     * @return string
     */
    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . 'responsive-images/';
    }
}
