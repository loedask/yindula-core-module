<?php

namespace Modules\YindulaCore\Utilities;


use App\Models\User;
use Illuminate\Support\Str;

/**
 * Generates Widget Text
 * @return string
 */
class WidgetData
{

    private $count;
    private $modelName;
    private $icon = "";
    private $link = "";

    public function __construct($count, $modelName, $icon, $link = "")
    {
        $this->count = $count;
        $this->modelName = $modelName;
        $this->icon = $icon;
        $this->link = $link;
    }

    /**
     * Make the Model Name first character uppercase.
     * @return string
     */
    protected function capitalizedTitle()
    {
        return Str::ucfirst($this->modelName);
    }

    /**
     * Get the plural form of the Model Name.
     * @return string
     */
    protected function pluralTitle()
    {
        return Str::plural($this->capitalizedTitle());
    }

    protected function dynamicTitle()
    {
        return $this->count <= 1 ? $this->capitalizedTitle() : $this->pluralTitle();
    }

    protected function fullTitle()
    {
        return "{$this->count} {$this->dynamicTitle()}";
    }

    /**
     * Convert the given string to lower-case.
     * @return string
     */
    protected function lowerFullTitle()
    {
        return Str::lower($this->fullTitle());
    }

    /**
     * Convert the given string to lower-case.
     * @return string
     */
    protected function lowerDynamicTitle()
    {
        return Str::lower($this->dynamicTitle());
    }

    /**
     * Convert the given Model Name to lower-case.
     * @return string
     */
    protected function lowerPluralTitle()
    {
        return Str::lower($this->pluralTitle());
    }


    public function getTitle()
    {
        return $this->count;
    }

    public function getText()
    {
        return $this->dynamicTitle();
    }

    public function getButtonText()
    {
        return "View " . $this->dynamicTitle();
    }

    public function getLink()
    {
        if ($this->link == null) {
            return  "admin.dashboard";
        }

        return $this->link;
    }

    public function getIcon()
    {
        return $this->icon;
    }
}
