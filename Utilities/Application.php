<?php

namespace Modules\YindulaCore\Utilities;

use Illuminate\Support\Facades\App;

class Application
{

    public $value;

    /**
     * Application environment.
     *
     * @var string
     */
    const ENVIRONMENT_LIVE = 'production';


    /**
     * Get the version number of the application.
     *
     * @return string
     */
    public static function version()
    {
        return exec('git describe --tags');
    }


    public static function languages()
    {

        $directory     = base_path() . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR;

        $glob    = glob($directory . "*", GLOB_ONLYDIR);

        $array_language = array_map(
            function ($value) use ($directory) {
                return str_replace($directory, '', $value);
            },
            $glob
        );

        $array_language = array_map(
            function ($value) use ($directory) {
                return preg_replace('/[0-9]+/', '', $value);
            },
            $array_language
        );

        $array_language = array_filter($array_language);

        return $array_language;
    }


    /**
     * Get the name of the application.
     *
     * @return string
     */
    public function name()
    {
        return "Yindula Core";
    }


    /**
     * Get the name of the application.
     *
     * @return string
     */
    public function cssPathPrefix()
    {
        if (App::environment(['production', 'staging'])) {
            return "public/";
        }
        return "";
    }
}
