<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

use Faker\Factory as FakerFactory;

class RandomCatchPhraseGenerator
{
    /**
     * Generate a random catchphrase.
     *
     * @return string
     */
    public static function generate(): string
    {
        $faker = FakerFactory::create();
        return $faker->catchPhrase;
    }
}

