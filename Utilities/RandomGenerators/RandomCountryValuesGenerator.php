<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

use Modules\YindulaCore\Entities\Country;

class RandomCountryValuesGenerator
{
    /**
     * Generate a random title and description.
     *
     * @return array
     */
    public static function generate()
    {
        $country = Country::inRandomOrder()->first();

        return [
            'iso_2' => $country->iso_3166_2,
            'iso_3' => $country->iso_3166_3,
        ];
    }
}
