<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

/**
 * Class RandomFloatGenerator
 */
class RandomDateTimeGenerator
{
    /**
     * Generate a random date and time value.
     *
     * @param string $format The date and time format.
     * @return string
     */
    public static function generate(string $format = 'Y-m-d H:i:s'): string
    {
        return date($format, mt_rand(1, time()));
    }
}

