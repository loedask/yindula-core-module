<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

/**
 * Class RandomDigitNotNullGenerator
 */
class RandomDigitNotNullGenerator
{
    /**
     * Generate a random non-zero digit.
     *
     * @return int
     */
    public static function generate(): int
    {
        return mt_rand(1, 9);
    }
}

