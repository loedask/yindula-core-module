<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

use Faker\Factory;

class RandomUniqueNumberGenerator
{
    protected static $usedNumbers = [];

    /**
     * Generate a unique random number.
     *
     * @param int $min The minimum value of the random number.
     * @param int $max The maximum value of the random number.
     * @return int
     */
    public static function generate(int $min = 0, int $max = 9999): int
    {
        $faker = Factory::create();

        do {
            $number = $faker->numberBetween($min, $max);
        } while (in_array($number, static::$usedNumbers));

        static::$usedNumbers[] = $number;

        return $number;
    }
}
