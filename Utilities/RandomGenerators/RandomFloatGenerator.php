<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

/**
 * Class RandomFloatGenerator
 */
class RandomFloatGenerator
{
    /**
     * Generate a random float number within the specified range.
     *
     * @param float $min The minimum value.
     * @param float $max The maximum value.
     * @param int $decimals The number of decimal places.
     * @return float
     */
    public static function generate(float $min, float $max, int $decimals = 2): float
    {
        $randomNumber = mt_rand() / mt_getrandmax();
        $randomFloat = $min + $randomNumber * ($max - $min);

        return round($randomFloat, $decimals);
    }
}
