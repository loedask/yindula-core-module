<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

use Illuminate\Support\Str;
use Faker\Factory as FakerFactory;

class RandomRealTextGenerator
{
    /**
     * Generate a random title and description.
     *
     * @param int $minWords Minimum number of words in the title.
     * @param int $maxWords Maximum number of words in the title.
     * @param int $minChars Minimum number of characters in the description.
     * @param int $maxChars Maximum number of characters in the description.
     * @return array
     */
    public static function generate($minWords = 2, $maxWords = 3, $minChars = 10, $maxChars = 250)
    {
        $faker = FakerFactory::create();

        $real_description = $faker->realText($faker->numberBetween($minChars, $maxChars));

        // Generate title without ellipsis
        $real_title = Str::of($real_description)
            ->words($faker->numberBetween($minWords, $maxWords))
            ->replaceLast('...', '') // Remove trailing ellipsis if present
            ->stripTags();

        return [
            'title' => $real_title,
            'description' => $real_description,
        ];
    }
}
