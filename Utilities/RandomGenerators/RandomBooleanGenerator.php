<?php

namespace Modules\YindulaCore\Utilities\RandomGenerators;

/**
 * Class RandomFloatGenerator
 */
class RandomBooleanGenerator
{
    /**
     * Generate a random boolean value.
     *
     * @return bool
     */
    public static function generate(): bool
    {
        return mt_rand(0, 1) === 1;
    }
}

