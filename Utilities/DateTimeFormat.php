<?php

namespace Modules\YindulaCore\Utilities;

use Carbon\Carbon;

/**
 * Generates Widget Text
 * @return string
 */
class DateTimeFormat
{
    /**
     * date
     *
     * @var string
     */
    public $date;

    public function __construct($date = null)
    {
        $this->date = $date;
    }


    /**
     * This attribute converts the "date" into a more readable attribute.
     * @return void
     */
    public function getReadable()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('d M Y');
    }

    /**
     * This method converts the "date" into a more readable attribute.
     * It tells the time ago instead of the exact time.
     * @return void
     */
    public function getTimeAgo()
    {
        return Carbon::parse($this->date)->diffForHumans();
    }


    /**
     * This attribute converts the "date" into a more readable attribute.
     * @return void
     */
    public function getReadableYear()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('Y');
    }


    /**
     * This attribute converts the "date" into a more readable attribute.
     * @return void
     */
    public function getReadableMonth()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('M');
    }
}
