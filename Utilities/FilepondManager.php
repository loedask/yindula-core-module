<?php

namespace Modules\YindulaCore\Utilities;

use RahulHaque\Filepond\Facades\Filepond;

/**
 * Generates Widget Text
 * @return string
 */
class FilepondManager
{
    /**
     * file
     *
     * @var mixed
     */
    public $file;

    public function __construct($file = null)
    {
        $this->file = $file;
    }

    public function getStoredFile()
    {
        return  Filepond::field($this->file)->getFile();
    }

    protected function deleteStoredFile()
    {
        return Filepond::field($this->file)->delete();
    }
}
