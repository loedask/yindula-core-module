<?php

use Illuminate\Support\Facades\Route;
use Modules\YindulaCore\app\Http\Controllers\YindulaCoreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('yindula')->middleware('auth')->name('yindula.')->group(function () {
    Route::get('/', 'YindulaCoreController@index');

    Route::get('dashboard', [
        Modules\YindulaCore\app\Http\Controllers\YindulaCoreController::class, 'dashboard'
    ])->name('dashboard');

    Route::get('change-language/{lang}', [
        Modules\YindulaCore\app\Http\Controllers\YindulaCoreController::class, 'changeLanquage'
    ])->name('change.language');
});
