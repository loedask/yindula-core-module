<?php

namespace Modules\YindulaCore\Services;

use Carbon\Carbon;

class DateService
{
    public function today(): Carbon
    {
        return Carbon::today();
    }

    public function formatDate(Carbon $date, string $format = 'Y-m-d'): string
    {
        return $date->format($format);
    }
}
