<?php

namespace Modules\YindulaCore\app\Console\Traits;

use Modules\YindulaCore\app\Console\Commands\AddContentToFrontendBlade;
use Modules\YindulaCore\app\Console\Commands\AddContentToSiteBlade;
use Modules\YindulaCore\app\Console\Commands\AddModuleInstallerPlugin;
use Modules\YindulaCore\app\Console\Commands\CreateFrontPagesModule;
use Modules\YindulaCore\app\Console\Commands\GenerateMenuFiles;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallDevPackages;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallDoctrineDbal;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallEnumPackage;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallHtmlPackage;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallLivewire;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallLivewireModules;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallLivewireTables;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallPermissions;
use Modules\YindulaCore\app\Console\Commands\Packages\InstallSluggablePackage;
use Modules\YindulaCore\app\Console\Commands\UpdateComposerAutoload;
use Modules\YindulaCore\app\Console\Commands\UpdateRouteServiceProvider;
use Modules\YindulaCore\app\Console\Commands\UpdateUserFactory;
use Modules\YindulaCore\app\Console\Commands\UpdateUserModelCommand;
use Modules\YindulaCore\app\Console\RunYindulaSetupCommands;


trait RegistersCommandsTrait
{
    /**
     * Register commands in the format of Command::class
     */
    protected function registerCommands(): void
    {
        $this->commands([
            RunYindulaSetupCommands::class,
            UpdateComposerAutoload::class,
            CreateFrontPagesModule::class,
            UpdateUserModelCommand::class,
            UpdateUserFactory::class,
            AddModuleInstallerPlugin::class,
            AddContentToSiteBlade::class,
            AddContentToFrontendBlade::class,
            UpdateRouteServiceProvider::class,
            InstallLivewire::class,
            InstallLivewireModules::class,
            InstallSluggablePackage::class,
            InstallHtmlPackage::class,
            InstallEnumPackage::class,
            InstallDoctrineDbal::class,
            InstallDevPackages::class,
            GenerateMenuFiles::class,
            InstallPermissions::class,
            InstallLivewireTables::class,
        ]);
    }
}
