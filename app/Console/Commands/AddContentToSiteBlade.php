<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class AddContentToSiteBlade extends Command
{
    protected $signature = 'yindula:add-content-to-site-blade';
    protected $description = 'Add content to Modules/FrontPages/Resources/views/site.blade.php';

    public function handle()
    {
        $filePath = base_path('Modules/FrontPages/Resources/views/site.blade.php');

        $content = <<<BLADE
@extends('frontpages::layouts.frontend', ['is_home' => \$is_home])

@section('content')

    @if (\$is_home)

    @else

    @endif

@endsection
BLADE;

        File::append($filePath, $content);

        $this->info('Content added to site.blade.php');
    }
}

