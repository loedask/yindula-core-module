<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class AddContentToFrontendBlade extends Command
{
    protected $signature = 'yindula:add-content-to-frontend-blade';
    protected $description = 'Add content to Modules/FrontPages/Resources/views/layouts/frontend.blade.php';

    public function handle()
    {
        $filePath = base_path('Modules/FrontPages/Resources/views/layouts/frontend.blade.php');

        $content = <<<BLADE
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    @include('frontpages::partials.frontend_stylesheet')

    @yield('page_css')
    @yield('css')
</head>

<body class="">

    <nav class="navbar navbar-reverse navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand smooth" href="/">
                {{ yindula_setting('site', 'name') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto ml-lg-3 align-items-lg-center">
                    {{ yindula_menu('main', 'frontpages::menus.main_menu') }}
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    @include('frontpages::partials.footer')
    @include('frontpages::partials.copyright')

    @include('frontpages::partials.frontend_script')
</body>

@yield('page_js')
@yield('scripts')

</html>
BLADE;

        File::put($filePath, $content);

        $this->info('Content added to frontend.blade.php');
    }
}
