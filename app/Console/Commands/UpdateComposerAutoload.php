<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;

class UpdateComposerAutoload extends Command
{
    protected $signature = 'yindula:update-composer-autoload';
    protected $description = 'Update the composer.json file for PSR-4 autoloading';

    public function handle()
    {
        // Read the composer.json file
        $this->info('--- Reading the composer.json file ---');
        $composerJsonPath = base_path('composer.json');
        $composerJson = json_decode(file_get_contents($composerJsonPath), true);

        // Check if "Modules\\" is already present in the PSR-4 autoload
        if (!isset($composerJson['autoload']['psr-4']['Modules\\'])) {

            // Add "Modules\\" after "App\\"
            $this->info('--- Adding "Modules\\" after "App\\ ---');
            $psr4Autoload = $composerJson['autoload']['psr-4'];
            $newPsr4Autoload = [];
            foreach ($psr4Autoload as $namespace => $path) {
                $newPsr4Autoload[$namespace] = $path;
                if ($namespace === 'App\\') {
                    $newPsr4Autoload['Modules\\'] = 'Modules/';
                }
            }

            // Update the composer.json file with the modified autoloading
            $this->info('--- Updating the composer.json file with the modified autoloading ---');
            $composerJson['autoload']['psr-4'] = $newPsr4Autoload;
            file_put_contents($composerJsonPath, json_encode($composerJson, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

            // Dump the autoload files
            $this->info('--- Dumping the autoload files ---');
            $composer = new Composer($this->laravel['files']);
            $composer->dumpAutoloads();

            $this->info('Composer autoload updated successfully!');
        } else {
            $this->info('"Modules\\" entry already present in the PSR-4 autoload. No changes needed.');
        }
    }
}
