<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\File;

class UpdateRouteServiceProvider extends Command
{
    protected $signature = 'yindula:update-route-service-provider';
    protected $description = 'Replace the RouteServiceProvider namespace in app\Providers\RouteServiceProvider.php';

    public function handle()
    {
        $routeServiceProviderPath = app_path('Providers\RouteServiceProvider.php');

        if (File::exists($routeServiceProviderPath)) {
            $content = File::get($routeServiceProviderPath);

            // Add the "use Modules\YindulaCore\Enums\HomeEnum;" statement at the top
            $content = preg_replace('/(namespace .+?;)/', "$1\n\nuse Modules\\YindulaCore\\Enums\\HomeEnum;", $content);

            // Check if the string exists in the file
            $this->info('--- Checking if the string exists in the file ---');
            if (strpos($content, "public const HOME = '/home'") !== false) {

                // Replace the string with the new one
                $this->info('--- Replacing the string with the new one ---');
                $newContent = str_replace(
                    "public const HOME = '/home';",
                    'public const HOME = HomeEnum::YINDULA_DASHBOARD;',
                    $content
                );

                // Save the modified content back to the file
                $this->info('--- Saving the modified content back to the file ---');
                File::put($routeServiceProviderPath, $newContent);

                $this->info('Replacement completed successfully.');
            } else {
                $this->error('The specified string was not found in the RouteServiceProvider.');
            }
        } else {
            $this->error('RouteServiceProvider file not found.');
        }

    }
}
