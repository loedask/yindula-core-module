<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\File;

class UpdateUserModelCommand extends Command
{
    protected $signature = 'yindula:update-user-model';
    protected $description = 'Replace the User model namespace in App\Models\User';

    public function handle()
    {
        $userModelPath = app_path('Models/User.php');

        if (File::exists($userModelPath)) {
            $content = File::get($userModelPath);

            // Check if the string exists in the file
            $this->info('--- Checking if the string exists in the file ---');
            if (strpos($content, 'use Illuminate\Foundation\Auth\User as Authenticatable') !== false) {

                // Replace the string with the new one
                $this->info('--- Replacing the string with the new one ---');
                $newContent = str_replace(
                    'use Illuminate\Foundation\Auth\User as Authenticatable;',
                    'use Modules\YindulaCore\Entities\User as Authenticatable;',
                    $content
                );

                // Save the modified content back to the file
                $this->info('--- Saving the modified content back to the file ---');
                File::put($userModelPath, $newContent);

                $this->info('Replacement completed successfully.');
            } else {
                $this->error('The specified string was not found in the User model.');
            }
        } else {
            $this->error('User model file not found.');
        }
    }
}
