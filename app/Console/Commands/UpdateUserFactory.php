<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\File;

class UpdateUserFactory extends Command
{
    protected $signature = 'yindula:update-user-factory';
    protected $description = 'Update UserFactory.php content';

    public function handle()
    {
        $path = database_path('factories/UserFactory.php');
        $content = <<<'PHP'
        <?php

        namespace database\factories;

        use Modules\YindulaCore\Database\factories\UserFactory as Factory;

        /**
         * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
         */
        class UserFactory extends Factory
        {
        }
        PHP;

        File::put($path, $content);

        $this->info('UserFactory.php updated successfully!');
    }
}
