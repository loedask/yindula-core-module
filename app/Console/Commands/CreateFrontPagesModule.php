<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class CreateFrontPagesModule extends Command
{
    protected $signature = 'yindula:create-front-pages-module';
    protected $description = 'Create the FrontPages module';

    public function handle()
    {
        $moduleName = 'FrontPages';
        $modulePath = base_path("Modules/{$moduleName}");

        // Check if the module directory already exists
        $this->info('--- Checking if the module directory already exists ---');
        if (File::exists($modulePath)) {
            $this->error("Module '{$moduleName}' already exists!");
            return;
        }

        // Run the 'php artisan module:make' command
        $this->info("--- Running the 'php artisan module:make' command ---");
        $this->call('module:make', ['name' => [$moduleName]]);

        // Replace the contents of FrontPagesController with the provided code
        $this->info("--- Replacing the contents of FrontPagesController ---");
        $this->replaceControllerContents($moduleName);

        // Create site.blade.php in the views directory
        $this->info("--- Creating site.blade.php in the views directory ---");
        $this->createSiteBladeView($moduleName);

        // Remove index.blade.php if it exists
        $this->info("--- Removing index.blade.php if it exists ---");
        $this->removeIndexBladeView($moduleName);

        // Create frontend.blade.php in the views directory
        $this->info("--- Creating frontend.blade.php in the views directory ---");
        $this->createFontendBladeView($moduleName);

        // Remove master.blade.php if it exists
        $this->info("--- Removing master.blade.php if it exists ---");
        $this->removeMasterBladeView($moduleName);

        // Create files in the Partials views directory
        $this->info("--- Creating files in the Partials views directory ---");
        $this->createBladeViewFilesPartials($moduleName);

        $this->info("Module '{$moduleName}' created successfully!");
    }

    protected function replaceControllerContents($moduleName)
    {
        $controllerPath = base_path("Modules/{$moduleName}/app/Http/Controllers/FrontPagesController.php");
        $newControllerContents = "<?php

namespace Modules\\{$moduleName}\\app\\Http\\Controllers;

use Modules\\YindulaSite\\Http\\Controllers\\YindulaSiteController as Controller;

class FrontPagesController extends Controller
{
}";

        // Check if the controller file exists before making changes
        $this->info("--- Checking if the controller file exists before making changes ---");
        if (File::exists($controllerPath)) {
            File::put($controllerPath, $newControllerContents);
            $this->info("FrontPagesController contents replaced successfully!");
        } else {
            $this->error("FrontPagesController not found!");
        }
    }

    protected function createSiteBladeView($moduleName)
    {
        $viewsPath = base_path("Modules/{$moduleName}/resources/views");
        $siteBladeContents = "";

        // Check if the views directory exists before creating the file
        $this->info("--- Checking if the views directory exists before creating the file ---");
        if (File::isDirectory($viewsPath)) {
            File::put("$viewsPath/site.blade.php", $siteBladeContents);
            $this->info("site.blade.php created successfully!");
        } else {
            $this->error("Views directory not found!");
        }
    }

    protected function removeIndexBladeView($moduleName)
    {
        $viewsPath = base_path("Modules/{$moduleName}/resources/views");
        $indexBladePath = "$viewsPath/index.blade.php";

        // Check if the index.blade.php file exists before removing
        $this->info("--- Checking if the index.blade.php file exists before removinge ---");
        if (File::exists($indexBladePath)) {
            File::delete($indexBladePath);
            $this->info("index.blade.php removed successfully!");
        } else {
            $this->info("index.blade.php not found. Nothing to remove.");
        }
    }

    protected function createFontendBladeView($moduleName)
    {
        $viewsPath = base_path("Modules/{$moduleName}/resources/views/layouts");
        $frontendBladeContents = "<!-- Contents of frontend.blade.php -->";

        // Check if the views directory exists before creating the file
        $this->info("--- Checking if the views directory exists before creating the file ---");
        if (File::isDirectory($viewsPath)) {
            File::put("$viewsPath/frontend.blade.php", $frontendBladeContents);
            $this->info("frontend.blade.php created successfully!");
        } else {
            $this->error("Views directory not found!");
        }
    }

    protected function removeMasterBladeView($moduleName)
    {
        $viewsPath = base_path("Modules/{$moduleName}/resources/views/layouts");
        $masterBladePath = "$viewsPath/master.blade.php";

        // Check if the master.blade.php file exists before removing
        $this->info("--- Checking if the master.blade.php file exists before removinge ---");
        if (File::exists($masterBladePath)) {
            File::delete($masterBladePath);
            $this->info("master.blade.php removed successfully!");
        } else {
            $this->info("master.blade.php not found. Nothing to remove.");
        }
    }

    protected function createBladeViewFilesPartials($moduleName)
    {
        $partialsPath = base_path("Modules/{$moduleName}/resources/views/partials");
        $bladeFiles = [
            'copyright.blade.php' => '<!-- Contents of copyright.blade.php -->',
            'footer.blade.php' => '<!-- Contents of footer.blade.php -->',
            'frontend_script.blade.php' => '<!-- Contents of frontend_script.blade.php -->',
            'frontend_stylesheet.blade.php' => '<!-- Contents of frontend_stylesheet.blade.php -->',
        ];

        // Check if the partials directory exists, and create it if not
        if (!File::isDirectory($partialsPath)) {
            // Create the directory if it does not exist
            File::makeDirectory($partialsPath, 0755, true, true);
            $this->info("Partials directory created successfully!");
        }

        // Create blade files in the partials directory
        foreach ($bladeFiles as $filename => $content) {
            File::put("$partialsPath/$filename", $content);
            $this->info("$filename created successfully!");
        }
    }
}
