<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;

class InstallHtmlPackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-sluggable-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install cviebrock/eloquent-sluggable package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing cviebrock/eloquent-sluggable...');

        // Execute Composer command to require the package
        exec('composer require cviebrock/eloquent-sluggable');

        // Display a success message once the package is installed
        $this->info('cviebrock/eloquent-sluggable installed successfully!');
    }
}
