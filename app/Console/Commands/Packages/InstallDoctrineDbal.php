<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;

class InstallDoctrineDbal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-doctrine-dbal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install doctrine/dbal package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing doctrine/dbal...');

        // Execute Composer command to require the package
        exec('composer require doctrine/dbal:3.6');

        // Display a success message once the package is installed
        $this->info('doctrine/dbal installed successfully!');
    }
}
