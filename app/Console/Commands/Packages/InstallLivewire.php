<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallLivewire extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'yindula:install-livewire';

    /**
     * The console command description.
     */
    protected $description = 'Install the Livewire package';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Installing Livewire...');
        // Run the Composer command to install Livewire
        exec('composer require livewire/livewire');

        // Publish Livewire assets npm install livewire-sortable
        $this->info('Publishing Livewire assets...');
        exec('php artisan livewire:publish --assets');

        //  Install the livewire-sortable package
        $this->info('installing the livewire-sortable package...');
        exec('npm install livewire-sortable');

        // Add import statement to resources/js/app.js
        $this->addLivewireSortableImport();

        $this->info('Livewire installed successfully!');
    }

    /**
     * Add import statement for livewire-sortable to resources/js/app.js
     */
    private function addLivewireSortableImport()
    {
        $appJsPath = base_path('resources/js/app.js');
        $livewireSortableImport = "import 'livewire-sortable';";

        // Check if the import statement already exists in the app.js file
        $existingContent = File::get($appJsPath);
        if (strpos($existingContent, $livewireSortableImport) === false) {
            // If not, add the import statement to the file
            File::append($appJsPath, "\n" . $livewireSortableImport);
            $this->info('livewire-sortable import statement added to resources/js/app.js');
        } else {
            $this->info('livewire-sortable import statement already exists in resources/js/app.js');
        }
    }

}
