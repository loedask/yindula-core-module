<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;

class InstallLivewireModules extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'yindula:install-livewire-modules';

    /**
     * The console command description.
     */
    protected $description = 'Install Livewire and mhmiton/laravel-modules-livewire packages';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Installing nwidart/laravel-modules...');
        exec('composer require nwidart/laravel-modules');

        $this->info('Publishing nwidart/laravel-modules...');
        exec("php artisan vendor:publish --provider='Nwidart\Modules\LaravelModulesServiceProvider'");

        $this->info('Publishing vite-modules-loader.js...');
        exec("php artisan vendor:publish --provider='Nwidart\Modules\LaravelModulesServiceProvider' --tag='vite'");

        $this->info('nwidart/laravel-modules installed successfully!');

        $this->info('Installing mhmiton/laravel-modules-livewire...');
        exec('composer require mhmiton/laravel-modules-livewire');

        $this->info('Publishing mhmiton/laravel-modules-livewire assets...');
        exec("php artisan vendor:publish --provider='Mhmiton\LaravelModulesLivewire\LaravelModulesLivewireServiceProvider'");

        $this->info('mhmiton/laravel-modules-livewire installed successfully!');
    }
}
