<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class InstallFlashPackage extends Command
{/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-flash-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install laracasts/flash package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing laracasts/flash...');

        // Execute Composer command to require the package
        exec('composer require laracasts/flash');

        // Display a success message once the package is installed
        $this->info('laracasts/flash installed successfully!');
    }
}
