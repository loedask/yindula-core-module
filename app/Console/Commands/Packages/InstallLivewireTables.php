<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallLivewireTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-livewire-tables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install rappasoft/laravel-livewire-tables package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing rappasoft/laravel-livewire-tables...');

        // Execute Composer command to require the package
        exec('composer require rappasoft/laravel-livewire-tables');

        // Display a success message once the package is installed
        $this->info('rappasoft/laravel-livewire-tables installed successfully!');

        // Publish the Livewire Tables configuration
        $this->info('Publishing Livewire Tables configuration...');
        exec('php artisan vendor:publish --provider="Rappasoft\LaravelLivewireTables\LaravelLivewireTablesServiceProvider" --tag=livewire-tables-config');

        // Display a success message once the configuration is published
        $this->info('Livewire Tables configuration published successfully!');
        
        // Update Livewire Tables configuration
        $this->updateLivewireTablesConfig();

        // Display a final success message
        $this->info('Livewire Tables configuration updated successfully!');

        
    }


    /**
     * Update Livewire Tables configuration file.
     *
     * @return void
     */
    protected function updateLivewireTablesConfig()
    {
        $configPath = config_path('livewire-tables.php');

        // Check if the livewire-tables.php config file exists
        if (File::exists($configPath)) {
            // Read the current content of livewire-tables.php
            $configContent = File::get($configPath);

            // Replace 'theme' => 'tailwind' with 'theme' => 'bootstrap-4'
            $updatedContent = str_replace(
                "'theme' => 'tailwind'",
                "'theme' => 'bootstrap-4'",
                $configContent
            );

            // Write the updated content back to livewire-tables.php
            File::put($configPath, $updatedContent);
        } else {
            // Display a warning if the config file doesn't exist
            $this->warn('livewire-tables.php configuration file not found.');
        }
    }

}
