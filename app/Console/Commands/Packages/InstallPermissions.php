<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class InstallPermissions  extends Command
{/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install spatie/laravel-permission package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing spatie/laravel-permission...');

        // Execute Composer command to require the package
        exec('composer require spatie/laravel-permission');

        // Display a success message once the package is installed
        $this->info('spatie/laravel-permission installed successfully!');
    }
}
