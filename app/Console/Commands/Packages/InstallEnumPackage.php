<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class InstallEnumPackage extends Command
{/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-enum-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install bensampo/laravel-enum package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing bensampo/laravel-enum...');

        // Execute Composer command to require the package
        exec('composer require bensampo/laravel-enum');

        // Display a success message once the package is installed
        $this->info('bensampo/laravel-enum installed successfully!');
    }
}
