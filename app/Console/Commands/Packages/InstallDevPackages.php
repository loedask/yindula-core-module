<?php

namespace Modules\YindulaCore\app\Console\Commands\Packages;

use Illuminate\Console\Command;

class InstallDevPackages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:install-dev-packages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install development packages in require-dev';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // List of packages to be installed in require-dev
        $packages = [
           // Laravel DebugBar for debugging
           "barryvdh/laravel-debugbar",

           // Laravel IDE Helper for better IDE support
           "barryvdh/laravel-ide-helper",

           // Laravel Query Detector for detecting N+1 queries
           "beyondcode/laravel-query-detector",

           // Laravel Microscope for code analysis
           "imanghafoori/laravel-microscope",

           // Laravel Generator for scaffolding
           "infyomlabs/laravel-generator",

           // ISeed for database seeding
           "orangehill/iseed",

           // Laravel Countries for country-related data
           "webpatser/laravel-countries",

           // Laravel Translatable String Exporter for exporting translation strings
           "kkomelin/laravel-translatable-string-exporter",

           // Laravel Migrations Generator for generating migrations from database tables
           "kitloong/laravel-migrations-generator",

           // Laravel Migrations Generator for generating migrations from database tables
           "infyomlabs/laravel-generator",
        ];

        // Display a message indicating the installation process has started
        $this->info('Installing development packages in require-dev...');

        // Execute Composer command to require the specified packages in require-dev
        exec('composer require --dev ' . implode(' ', $packages));

        // Display a success message once the packages are installed
        $this->info('Development packages in require-dev installed successfully!');
    }
}
