<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;

class AddModuleInstallerPlugin extends Command
{
    protected $signature = 'yindula:add-module-installer';
    protected $description = 'Add joshbrw/laravel-module-installer to allow-plugins in composer.json';

    public function handle()
    {
        $composerJsonPath = base_path('composer.json');

        $composerJson = json_decode(file_get_contents($composerJsonPath), true);

        // Add joshbrw/laravel-module-installer to the allow-plugins section
        $composerJson['config']['allow-plugins']['joshbrw/laravel-module-installer'] = true;

        // Save the updated composer.json file without escaping slashes
        file_put_contents($composerJsonPath, json_encode($composerJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        $this->info('joshbrw/laravel-module-installer added to allow-plugins in composer.json');
    }
}
