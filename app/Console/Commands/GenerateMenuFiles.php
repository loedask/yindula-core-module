<?php

namespace Modules\YindulaCore\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class GenerateMenuFiles extends Command
{
    
    protected $signature = 'yindula:generate-menu-files';

    protected $description = 'Generate menu files in Modules/FrontPages/resources/views/menus';

    public function handle()
    {
        // Specify the folder path
        $folderPath = base_path("Modules/FrontPages/resources/views/menus");

        // Create the 'menus' folder if it doesn't exist
        if (!File::exists($folderPath)) {
            File::makeDirectory($folderPath, 0755, true, true);
            $this->info('Created "menus" folder');
        }

        // Generate menu files
        $this->generateIfNotExists('bootstrap.blade.php', $this->getBootstrapBladeContent());
        $this->generateIfNotExists('footer_menu.blade.php', $this->getFooterMenuBladeContent());
        $this->generateIfNotExists('main_menu.blade.php', $this->getMainMenuBladeContent());

        $this->info('Menu files generated successfully!');
    }

    protected function generateIfNotExists($filename, $content)
    {
        $path = base_path("Modules/FrontPages/resources/views/menus/{$filename}");

        if (!File::exists($path)) {
            File::put($path, $content);
            $this->info("Generated: {$filename}");
        } else {
            $this->info("Skipped: {$filename} (already exists)");
        }
    }

    protected function getBootstrapBladeContent()
    {
        return <<<BLADE
    @if(!isset(\$innerLoop))
        <ul class="navbar-nav mr-auto">
    @else
        <ul class="dropdown-menu">
    @endif

    @php
        // Check if the items are translatable and load translations if necessary
        if (Voyager::translatable(\$items)) {
            \$items = \$items->load('translations');
        }
    @endphp

    @foreach (\$items as \$item)
        @php
            // Preserve the original item and translate if necessary
            \$originalItem = \$item;
            if (Voyager::translatable(\$item)) {
                \$item = \$item->translate(\$options->locale);
            }

            \$listItemClass = null;
            \$linkAttributes =  null;
            \$styles = null;
            \$icon = null;
            \$caret = null;

            // Background Color or Color
            if (isset(\$options->color) && \$options->color == true) {
                \$styles = 'color:' . \$item->color;
            }
            if (isset(\$options->background) && \$options->background == true) {
                \$styles = 'background-color:' . \$item->color;
            }

            \$linkAttributes = 'class="nav-link"';

            // With Children Attributes
            if (!\$originalItem->children->isEmpty()) {
                \$linkAttributes = 'class="dropdown-toggle nav-link" data-toggle="dropdown"';
                \$caret = '<span class="caret"></span>';

                // Check if the current URL matches the item's link
                if (url(\$item->link()) == url()->current()) {
                    \$listItemClass = 'dropdown active';
                } else {
                    \$listItemClass = 'dropdown';
                }
            }

            // Set Icon
            if (isset(\$options->icon) && \$options->icon == true) {
                \$icon = '<i class="' . \$item->icon_class . '"></i>';
            }

            // Adjust link attributes if inner loop
            if (isset(\$innerLoop)):
                \$linkAttributes = 'class="dropdown-item"';
            endif;
        @endphp

        <li class="{{ \$listItemClass }} nav-item">
            <a href="{{ url(\$item->link()) }}" target="{{ \$item->target }}" style="{{ \$styles }}" {!! isset(\$linkAttributes) ? \$linkAttributes : '' !!}>
                {!! \$icon !!}
                <span>{{ \$item->title }}</span>
                {!! \$caret !!}
            </a>
            @if (!\$originalItem->children->isEmpty())
                @include('cms::menus.bootstrap', ['items' => \$originalItem->children, 'options' => \$options, 'innerLoop' => true])
            @endif
        </li>
    @endforeach

    </ul>
    BLADE;
    }


    protected function getFooterMenuBladeContent()
    {
        return <<<BLADE
    <ul class="posts">
        @foreach (\$items as \$menu_item)
            @php
                \$selectedItem = null;
                \$originalItem = \$menu_item;
    
                // Check if the menu item's link matches the current URL
                if (url(\$menu_item->link()) == url()->current()) {
                    \$selectedItem = 'active';
                }
            @endphp
    
            <li class="{{ \$selectedItem }}">
                <a href="{{ \$menu_item->link() }}">
                    {{ \$menu_item->title }}
                </a>
            </li>
        @endforeach
    </ul>
    BLADE;
    }
    

    protected function getMainMenuBladeContent()
    {
        return <<<BLADE
    @foreach (\$items as \$menu_item)
        @php
            \$selectedItem = null;
            \$originalItem = \$menu_item;
    
            // Check if the menu item's link matches the current URL
            if (url(\$menu_item->link()) == url()->current()) {
                \$selectedItem = 'active';
            }
        @endphp
        <li class="nav-item {{ \$selectedItem }}">
            <a href="{{ \$menu_item->link() }}" class="nav-link">
                <i class="fas fa-fire"></i>
                <span>{{ \$menu_item->title }}</span>
            </a>
            <ul class="dropdown-menu">
                @foreach (\$originalItem->children as \$item)
                    <li class="nav-item">
                        <a href="{{ \$item->link() }}" class="nav-link">
                            {{ \$item->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    @endforeach
    BLADE;
    }
    

}

