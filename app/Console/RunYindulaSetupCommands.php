<?php

namespace Modules\YindulaCore\app\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class RunYindulaSetupCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run setup commands: migrate, seed, passport:install, passport:keys';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-doctrine-dbal');
        
        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-livewire');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-livewire-modules');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-livewire-tables');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-sluggable-package');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-enum-package');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-permissions');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:install-dev-packages');

        // Run the 'php artisan update:composer-autoload' command
        $this->call('yindula:update-composer-autoload');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:create-front-pages-module');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:run-cms-setup');

       // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:update-user-model');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:update-user-factory');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:update-route-service-provider');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:seed-site-data');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:add-module-installer');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:add-content-to-site-blade');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:add-content-to-frontend-blade');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:generate-menu-files');

        // Run the 'php artisan module:make-and-check' command
        $this->call('yindula:install-fortify');

         // Run the 'php artisan module:make-and-check' command
        $this->call('bazin:run-all-npm');
    }

}

