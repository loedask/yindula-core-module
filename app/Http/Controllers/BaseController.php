<?php

namespace Modules\YindulaCore\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\YindulaCore\Traits\CanGetApiResponse;

/**
 * This class should be parent class for other API controllers
 * Class BaseController
 */
class BaseController extends Controller
{
    use CanGetApiResponse;

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 'Success',
            'token' => $token,
            'id' => auth()->user()->id,
            'token_type' => 'bearer',
            'minutes' => 60 * 24 * 7, // Set token lifetime to 1 week
        ]);
    }
}
