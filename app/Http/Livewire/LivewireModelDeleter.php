<?php

namespace Modules\YindulaCore\app\Http\Livewire;

use Livewire\Livewire;
use Laracasts\Flash\Flash;

class LivewireModelDeleter
{
    protected $model;
    protected $livewire;

    public function __construct($model)
    {
        $this->model = $model;
        $this->livewire = app(Livewire::class);
    }

    public function __invoke($id)
    {
        $this->model::find($id)->delete();
        Flash::success(__('messages.deleted', ['model' => __('models/pages.singular')]));
        $this->livewire->emit('refreshDatatable');
    }
}
