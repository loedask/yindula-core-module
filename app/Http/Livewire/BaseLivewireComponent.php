<?php

namespace Modules\YindulaCore\app\Http\Livewire;

use Livewire\Component;

abstract class BaseLivewireComponent extends Component
{
    /**
     * The model instance for the component.
     *
     * @var mixed
     */
    protected $model;

    /**
     * The component's state.
     *
     * @var array
     */
    public $state = [];

    /**
     * Get the validation rules for the component's state.
     *
     * @return array
     */
    abstract protected function rules();

    /**
     * Load the component's model instance.
     *
     * @return mixed
     */
    abstract protected function loadModel();

    /**
     * Initialize the component.
     *
     * @return void
     */
    public function mount()
    {
        $this->loadModel();
    }

    /**
     * Update the component's state.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function updated($key, $value)
    {
        $this->validateOnly($key, $this->rules());
    }

    /**
     * Save the component's state to the database.
     *
     * @return void
     */
    public function save()
    {
        $this->validate($this->rules());

        $this->model->update($this->state);

        $this->emit('saved');
    }

    /**
     * Delete the component's model instance from the database.
     *
     * @return void
     */
    public function delete()
    {
        $this->model->delete();

        $this->emit('deleted');
    }
}
