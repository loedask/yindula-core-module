<?php

namespace Modules\YindulaCore\app\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            \App::setLocale(\Auth::user()->language);
        } else {
            if (\Session::has('locale')) {
                \App::setLocale(\Session::get('locale'));
            }
        }

        return $next($request);
    }
}
