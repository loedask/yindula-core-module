<?php

namespace Modules\YindulaCore\Enums;

use BenSampo\Enum\Enum;

final class HomeEnum extends Enum
{
    public const YINDULA_DASHBOARD = '/yindula/dashboard';
}
