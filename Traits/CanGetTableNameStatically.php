<?php

namespace Modules\YindulaCore\Traits;

use Illuminate\Support\Str;

trait CanGetTableNameStatically
{
    public static function tableName()
    {
        return (new static)->getTable();
    }

    /**
     * Get the singular form of the Model Name.
     * @return string
     */
    public static function tableNameSingular()
    {
        return Str::singular(static::tableName());
    }

    /**
     * Make the Model Name first character uppercase.
     * @return string
     */
    public static function tableNameCapitalized(): string
    {
        return Str::ucfirst(static::tableNameSingular());
    }
}
