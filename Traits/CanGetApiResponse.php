<?php

namespace Modules\YindulaCore\Traits;

use Illuminate\Support\Facades\Response;
use Modules\YindulaCore\Utilities\ApiResponser;

trait CanGetApiResponse
{

    public function sendResponse($result, $message)
    {
        return Response::json(ApiResponser::makeResponse($message, $result));
    }

    public function sendError($error = null, $code = 404)
    {
        if ($error == null) {
            $error = 'Something went Wrong! Please try Again.';
        }
        return Response::json(ApiResponser::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
