<li class="side-menus {{ Request::is('yindulacv*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('yindula.dashboard') }}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

@if (Module::find('yindulacv'))
    @include('yindulacv::partials.menu')
@endif
