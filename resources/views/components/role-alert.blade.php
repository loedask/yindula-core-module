<div class="col-12 mb-4">
    <div class="hero text-white hero-bg-image hero-bg-parallax">
        <div class="hero-inner text-center">
            <p class="lead">{{ auth()->user()->role_name }}</p>
        </div>
    </div>
</div>
