@if (session()->has('impersonate'))
    <div class="col-12 mb-4">
        <div class="hero text-white hero-bg-image hero-bg-parallax">
            <div class="hero-inner text-center">
                <h2>You are impersonating </h2>
                <p class="lead">{{ auth()->user()->full_name }}</p>
                <div class="mt-4">
                    <a href="{{ route('leave.impersonation') }}" class="btn btn-outline-white btn-lg btn-icon icon-left">
                        <i class="fas fa-sign-in-alt"></i>
                        Leave Impersonation
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif
