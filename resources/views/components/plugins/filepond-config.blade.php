<link href="https://unpkg.com/filepond@^4/dist/filepond.css" rel="stylesheet" />

<script src="https://unpkg.com/filepond@^4/dist/filepond.js"></script>

<script>
    // Set default FilePond options
    FilePond.setOptions({
        server: {
            process: "{{ config('filepond.server.process') }}",
            revert: "{{ config('filepond.server.revert') }}",
            headers: {
                'X-CSRF-TOKEN': "{{ @csrf_token() }}",
            }
        }
    });

    // Create the FilePond instance
    FilePond.create(document.querySelector('input[name="image"]'));
    // FilePond.create(document.querySelector('input[name="filename[]"]'));
</script>
