<?php

namespace Modules\YindulaCore\Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Backend\Utilities\GeneratePassword;

class DefaultUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(2)->create();
        User::factory()->admin()->create();
    }
}
