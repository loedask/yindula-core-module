# Yindula Core

## Description

YindulaCore is a Laravel module developed by Daskana. It serves as a repository for essential base classes used in Yindula projects, as well as other Laravel applications.

The creator designed this module to address the repetitive task of adding the same code to multiple applications. By centralizing the common classes, such as controllers, repositories, and more, YindulaCore streamlines the development process.

Instead of rewriting or copying these classes for each project, developers can simply utilize the module to access the necessary functionality. This saves time and effort by providing a consistent and organized foundation for Laravel applications.

## Prerequisites

- [Git](https://git-scm.com/downloads) installed.
- [Laravel Framework](https://laravel.com) installed.
- [Laravel Modules](https://github.com/nwidart/laravel-modules) installed.
- [Laravel Module Installer](https://github.com/joshbrw/laravel-module-installer) installed.

### Important Notice

1. Before proceeding, ensure that you have installed the "[nwidart/laravel-modules](https://github.com/nwidart/laravel-modules)" package in your Laravel project.
Follow the provided instructions to install this package, as it allows for module installation within Laravel projects.

2. Once the "nwidart/laravel-modules" package is installed, an additional step is required to install modules into the "Modules" directory of your project.

3. To facilitate the automatic movement of Yindula Core module files, you need to install the "[joshbrw/laravel-module-installer](https://github.com/joshbrw/laravel-module-installer)" composer plugin.

To install the plugin, open a terminal in the root directory of your Laravel project and execute the following command:

   ```bash
    composer require joshbrw/laravel-module-installer
   ```

4. After successfully installing the "joshbrw/laravel-module-installer" plugin, you can proceed with the installation of the Yindula Core module.
During the installation process, the Yindula Core module will be automatically placed in the "Modules" directory of your project.

Refer to the "Installation and Build" section for the appropriate command or instructions to install a specific module.
The "joshbrw/laravel-module-installer" plugin will handle the automatic movement of Yindula Core module files to the "Modules" directory.

## Installation and Build

To install Yindula Core for your application, follow these steps:

1. Open a terminal in the root directory of your application.

2. Use the following command to install Yindula Core:

   ```bash
   composer require loecoscorp/yindula-core-module
   ```

This command will download and install the Yindula Core module for your application.

Once the installation is complete, you can proceed with the configuration and usage of Yindula Core in your project.

