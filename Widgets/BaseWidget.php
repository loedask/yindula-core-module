<?php

namespace Modules\YindulaCore\Widgets;

use Arrilot\Widgets\AbstractWidget;

abstract class BaseWidget extends AbstractWidget
{
    /**
     * The number of seconds before each reload.
     * False means no reload at all.
     *
     * @var int|float|bool
     */
    public $reloadTimeout = false;

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
