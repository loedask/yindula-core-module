<?php

namespace Modules\YindulaCore\Tests;

use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Tests\TestCase;
use Tests\CreatesApplication;

abstract class BaseTestCase extends TestCase
{
    use CreatesApplication, LazilyRefreshDatabase;
}
