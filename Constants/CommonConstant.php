<?php

namespace Modules\YindulaCore\Constants;

class CommonConstant
{
    public const LOGIN_PATH = '/login';
    public const DEFAULT_EMAIL = 'african@engineer.com';
    public const DEFAULT_FULLNAME = 'African Engineer';
}
