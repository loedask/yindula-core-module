<?php

namespace Modules\YindulaCore\Constants;

class MessagesConstant
{
    public const MESSAGES_RETRIEVED = 'messages.retrieved';
    public const MESSAGES_SAVED = 'messages.saved';
    public const MESSAGES_NOT_FOUND = 'messages.not_found';
    public const MESSAGES_UPDATED = 'messages.updated';
    public const MESSAGES_DELETED = 'messages.deleted';
}
