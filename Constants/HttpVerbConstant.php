<?php

namespace Modules\YindulaCore\Constants;

class HttpVerbConstant
{
    // Define HTTP verb constants
    public const HTTP_GET = 'GET';
    public const HTTP_POST = 'POST';
    public const HTTP_PUT = 'PUT';
    public const HTTP_DELETE = 'DELETE';
}
