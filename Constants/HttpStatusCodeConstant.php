<?php

namespace Modules\YindulaCore\Constants;

class HttpStatusCodeConstant
{
    public const STATUS_OK = 200;
    public const STATUS_REDIRECT = 302;
    public const STATUS_BAD_REQUEST = 400;
    public const STATUS_FORBIDDEN = 403;
    public const STATUS_NOT_FOUND = 404;
    public const STATUS_TOCKEN_MISMATCH = 419;
}
