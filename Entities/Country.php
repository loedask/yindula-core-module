<?php

namespace Modules\YindulaCore\Entities;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCore\Traits\CanGetTableNameStatically;

/**
 * Class Country
 * @package Modules\Backend\Entities
 * @version March 16, 2022, 11:07 pm UTC
 *
 * @property string $capital
 * @property string $citizenship
 * @property string $country_code
 * @property string $currency
 * @property string $currency_code
 * @property string $currency_sub_unit
 * @property string $currency_symbol
 * @property integer $currency_decimals
 * @property string $full_name
 * @property string $iso_3166_2
 * @property string $iso_3166_3
 * @property string $name
 * @property string $region_code
 * @property string $sub_region_code
 * @property boolean $eea
 * @property string $calling_code
 * @property string $flag
 */
class Country extends Model
{

    use HasFactory, CanGetTableNameStatically;

    public $table = 'countries';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'capital',
        'citizenship',
        'country_code',
        'currency',
        'currency_code',
        'currency_sub_unit',
        'currency_symbol',
        'currency_decimals',
        'full_name',
        'iso_3166_2',
        'iso_3166_3',
        'name',
        'region_code',
        'sub_region_code',
        'eea',
        'calling_code',
        'flag'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'capital' => 'string',
        'citizenship' => 'string',
        'country_code' => 'string',
        'currency' => 'string',
        'currency_code' => 'string',
        'currency_sub_unit' => 'string',
        'currency_symbol' => 'string',
        'currency_decimals' => 'integer',
        'full_name' => 'string',
        'iso_3166_2' => 'string',
        'iso_3166_3' => 'string',
        'name' => 'string',
        'region_code' => 'string',
        'sub_region_code' => 'string',
        'eea' => 'boolean',
        'calling_code' => 'string',
        'flag' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'capital' => 'nullable|string|max:255',
        'citizenship' => 'nullable|string|max:255',
        'country_code' => 'required|string|max:3',
        'currency' => 'nullable|string|max:255',
        'currency_code' => 'nullable|string|max:255',
        'currency_sub_unit' => 'nullable|string|max:255',
        'currency_symbol' => 'nullable|string|max:3',
        'currency_decimals' => 'nullable|integer',
        'full_name' => 'nullable|string|max:255',
        'iso_3166_2' => 'required|string|max:2',
        'iso_3166_3' => 'required|string|max:3',
        'name' => 'required|string|max:255',
        'region_code' => 'required|string|max:3',
        'sub_region_code' => 'required|string|max:3',
        'eea' => 'required|boolean',
        'calling_code' => 'nullable|string|max:3',
        'flag' => 'nullable|string|max:6'
    ];


    /**
     * Permission values
     *
     * @var array
     */
    public static $permissions = [
        'Browse Country',
        'Read Country',
        'Edit Country',
        'Add Country',
        'Delete Country',
    ];
}
