<?php

namespace Modules\YindulaCore\Entities;

use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\YindulaCore\Database\factories\UserFactory;

/**
 * Class User
 * @package Modules\UserManagement\Entities
 * @version October 29, 2022, 6:11 am UTC
 *
 */
class User extends Authenticatable
{

    use HasRoles;

    protected $appends = ['full_name', 'role_name'];


    /**
     * @comment Get User's full name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * @comment Get User's role name
     *
     * @return void
     */
    public function getRoleNameAttribute()
    {
        // Check if the 'roles' table exists
        if (!Schema::hasTable('roles')) {
            return null;
        }

        $role = $this->roles()->first();

        if (!empty($role)) {
            return $role->name;
        }

        return null; // Return null if no role is found
    }



    public function selectedLanguage()
    {
        return $this->language;
    }


    /** @return UserFactory */
    protected static function newFactory()
    {
        return UserFactory::new();
    }
}
