<?php

namespace Modules\YindulaCore\View\Plugins;

use Illuminate\View\Component;

class LivewireSortableJs extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('yindulacore::components.plugins.livewire-sortable-js');
    }
}
